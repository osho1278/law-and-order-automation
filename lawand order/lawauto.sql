-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2012 at 01:17 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lawauto`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `Areaid` int(20) NOT NULL,
  `Areaname` varchar(20) NOT NULL,
  PRIMARY KEY (`Areaid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--


-- --------------------------------------------------------

--
-- Table structure for table `citizen`
--

CREATE TABLE IF NOT EXISTS `citizen` (
  `Profession` varchar(20) NOT NULL,
  `Areaid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citizen`
--


-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE IF NOT EXISTS `complaints` (
  `Description` varchar(20) NOT NULL,
  `Psid` int(20) NOT NULL,
  `Emailid` varchar(20) NOT NULL,
  `Complaintid` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL,
  PRIMARY KEY (`Complaintid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaints`
--


-- --------------------------------------------------------

--
-- Table structure for table `crimetype`
--

CREATE TABLE IF NOT EXISTS `crimetype` (
  `Crimetypeid` varchar(20) NOT NULL,
  `Crimename` varchar(20) NOT NULL,
  PRIMARY KEY (`Crimetypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crimetype`
--


-- --------------------------------------------------------

--
-- Table structure for table `criminals`
--

CREATE TABLE IF NOT EXISTS `criminals` (
  `Name` varchar(20) NOT NULL,
  `Criminalid` varchar(20) NOT NULL,
  `Lastseenat` varchar(20) NOT NULL,
  `Crimetypeid` varchar(20) NOT NULL,
  `Identitymarks` varchar(20) NOT NULL,
  PRIMARY KEY (`Criminalid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `criminals`
--


-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `Designationid` varchar(20) NOT NULL,
  `Designationname` varchar(20) NOT NULL,
  `Permission` varchar(20) NOT NULL,
  PRIMARY KEY (`Designationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designation`
--


-- --------------------------------------------------------

--
-- Table structure for table `fir`
--

CREATE TABLE IF NOT EXISTS `fir` (
  `Firno` int(20) NOT NULL,
  `Firsummary` varchar(20) NOT NULL,
  `Psid` varchar(20) NOT NULL,
  `Suspect` varchar(20) NOT NULL,
  PRIMARY KEY (`Firno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fir`
--


-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE IF NOT EXISTS `help` (
  `Helpid` varchar(202) NOT NULL,
  `Questions` varchar(20) NOT NULL,
  `Answers` varchar(20) NOT NULL,
  PRIMARY KEY (`Helpid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help`
--


-- --------------------------------------------------------

--
-- Table structure for table `laws/acts`
--

CREATE TABLE IF NOT EXISTS `laws/acts` (
  `Actid` int(20) NOT NULL,
  `Section` varchar(20) NOT NULL,
  `Action` varchar(20) NOT NULL,
  PRIMARY KEY (`Actid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laws/acts`
--


-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE IF NOT EXISTS `licenses` (
  `Reason` varchar(20) NOT NULL,
  `Psid` int(20) NOT NULL,
  `Licenseid` int(20) NOT NULL,
  `Positionername` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL,
  PRIMARY KEY (`Licenseid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `licenses`
--


-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Usertype` varchar(20) NOT NULL,
  `Confirmation` varchar(20) NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--


-- --------------------------------------------------------

--
-- Table structure for table `missingperson`
--

CREATE TABLE IF NOT EXISTS `missingperson` (
  `Name` varchar(20) NOT NULL,
  `Missingpersonid` int(20) NOT NULL,
  `Photo` varchar(20) NOT NULL,
  PRIMARY KEY (`Missingpersonid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `missingperson`
--


-- --------------------------------------------------------

--
-- Table structure for table `missingvaluables`
--

CREATE TABLE IF NOT EXISTS `missingvaluables` (
  `Lostplace` varchar(20) NOT NULL,
  `Missingvaluablesid` int(20) NOT NULL,
  PRIMARY KEY (`Missingvaluablesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `missingvaluables`
--


-- --------------------------------------------------------

--
-- Table structure for table `officers`
--

CREATE TABLE IF NOT EXISTS `officers` (
  `Designationid` varchar(20) NOT NULL,
  `Psid` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officers`
--


-- --------------------------------------------------------

--
-- Table structure for table `policestation`
--

CREATE TABLE IF NOT EXISTS `policestation` (
  `Psid` int(20) NOT NULL,
  `Areaid` int(20) NOT NULL,
  `Psname` varchar(20) NOT NULL,
  PRIMARY KEY (`Psid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policestation`
--


-- --------------------------------------------------------

--
-- Table structure for table `poll`
--

CREATE TABLE IF NOT EXISTS `poll` (
  `Question` varchar(20) NOT NULL,
  `Answers` varchar(20) NOT NULL,
  `Show/Hide` varchar(20) NOT NULL,
  `Count` varchar(20) NOT NULL,
  `Pollid` varchar(20) NOT NULL,
  PRIMARY KEY (`Pollid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll`
--


-- --------------------------------------------------------

--
-- Table structure for table `reporting lost/items`
--

CREATE TABLE IF NOT EXISTS `reporting lost/items` (
  `Losttime` varchar(20) NOT NULL,
  `Reportno` int(20) NOT NULL,
  `Reportedby` varchar(20) NOT NULL,
  `Reportedon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reporting lost/items`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Name` varchar(20) NOT NULL,
  `Userid` varchar(20) NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  PRIMARY KEY (`Userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

